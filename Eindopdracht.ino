#include <LiquidCrystal_I2C.h>
#include <TM1638plus.h>
#define STROBE_TM 4
#define CLOCK_TM 6
#define DIO_TM 7
#define BUTTON 8
#define BUZZER_PIN 9
#define RED 11
#define YELLOW 12
#define GREEN 13
#define JOYSTICK_X_PIN A1
#define JOYSTICK_Y_PIN A2
#define POTENTIOMETER_PIN A3
#define JOYSTICK_BUTTON_PIN 2
bool high_freq = false;
TM1638plus tm(STROBE_TM, CLOCK_TM, DIO_TM, high_freq);
LiquidCrystal_I2C lcd(0x27, 16, 2);

bool game1Done = false;
bool game2Done = false;
bool game3Done = false;
bool game4Done = false;

void setup() {
  Serial.begin(9600);
  lcd.init();
  lcd.backlight();
  pinMode(BUTTON, INPUT_PULLUP);
  pinMode(RED, OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(GREEN, OUTPUT);
  pinMode(JOYSTICK_BUTTON_PIN, INPUT_PULLUP);
  pinMode(BUZZER_PIN, OUTPUT);
  pinMode(POTENTIOMETER_PIN, INPUT);
  tm.displayBegin();
  tm.reset();
}

// overkoepelende loop die bijhoudt welke games voltooid zijn.
void loop() {
  if (!game1Done) {
    game1();
  } else if (!game2Done) {
    game2();
  } else if (!game3Done) {
    game3();
  } else if (!game4Done) {
    game4();
  }
  if (game1Done && game2Done && game3Done && game4Done) {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("You WON!!!");
    lcd.setCursor(0, 1);
    lcd.print("Good game!");
  }
}

// --------------------------------- GAME1 ---------------------------------------------------
// -------------------------------------------------------------------------------------------
bool gameStarted = false;
bool hasWon = false;
int currentGame = 1;
int targetLED;
unsigned long startTime;
unsigned long reactionTime;
unsigned long timeToReact = 750;
int NUM_LEDS = 8;
int NUM_GAMES = 4;

void game1() {
  lcd.setCursor(0, 0);
  lcd.print("Round 1");
  lcd.setCursor(0, 1);
  lcd.print("Press to start!");

  while (!game1Done) {
    if (hasWon) {
      game1Done = true;
      displayWinningMessage();
    }
    if (!gameStarted && digitalRead(BUTTON) == LOW) {
      startGame();
      delay(500);
    }

    if (gameStarted) {
      uint8_t buttonData = tm.readButtons();
      if (buttonData & (0x01 << targetLED)) {
        endGame();
      }
    }
  }
}
void displayWinningMessage() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Congratulations!");
  lcd.setCursor(0, 1);
  lcd.print("You won!");
  delay(4000);

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Next game");
  lcd.setCursor(0, 1);
  lcd.print("Incoming!");
  delay(2000);
}

void startGame() {
  gameStarted = true;
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Get ready...");
  delay(1000);
  for (int i = 3; i > 0; i--) {
    lcd.setCursor(0, 1);
    lcd.print(i);
    delay(1000);
  }
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Press button!");
  targetLED = random(NUM_LEDS);
  tm.setLED(targetLED, 1);
  startTime = millis();
}

void endGame() {
  reactionTime = millis() - startTime;
  tm.setLED(targetLED, 0);
  gameStarted = false;

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Reaction time:");
  lcd.setCursor(0, 1);
  lcd.print(reactionTime);
  lcd.print(" ms");
  delay(2000);

  if (reactionTime <= timeToReact) {
    currentGame++;
    timeToReact -= 25;
    if (currentGame > NUM_GAMES) {
      hasWon = true;
    }
  } else {
    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("Too slow,");
    lcd.setCursor(0, 1);
    lcd.print("try again!");
    delay(2000);
    currentGame = 1;
  }

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Round ");
  lcd.print(currentGame);
  lcd.setCursor(0, 1);
  lcd.print("Press to start!");
}
// --------------------------------- GAME2 ---------------------------------------------------
// -------------------------------------------------------------------------------------------
unsigned long startTime2;
bool gameRunning = false;
int spaceShuttlePosition = 0;
int bombPosition[3];
int score2 = 0;
int targetScore = 30;
byte spaceShuttle[8] = {
  B11100,
  B11100,
  B00011,
  B11111,
  B11111,
  B00011,
  B11100,
  B11100,
};

byte bomb[8] = {
  B00100,
  B01110,
  B01110,
  B11111,
  B11111,
  B01110,
  B01110,
  B00100,
};
void game2() {

  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Welcome to:");
  lcd.setCursor(0, 1);
  lcd.print("Space Racer!");
  delay(3000);
  digitalWrite(RED, HIGH);
  lcd.clear();
  startGame2();

  while (!game2Done) {
    if (gameRunning) {
      moveSpaceShuttle();
      generateBombs();
      moveBombs();
      checkCollision();
      updateScore();
    }
  }
}

void startGame2() {
  resetGame();
  countDownStoplight();
}


void resetGame() {
  gameRunning = false;
  spaceShuttlePosition = 0;
  bombPosition[0] = -1;
  bombPosition[1] = -1;
  score2 = 0;
  lcd.clear();
}

void countDownStoplight() {
  lcd.print("3...");
  tm.setLED(0, true);
  delay(1000);
  lcd.print("2...");
  tm.setLED(0, false);
  tm.setLED(1, true);
  digitalWrite(RED, LOW);
  delay(1000);
  lcd.print("1...");
  tm.setLED(1, false);
  tm.setLED(2, true);
  digitalWrite(YELLOW, HIGH);
  delay(1000);
  digitalWrite(YELLOW, LOW);
  lcd.setCursor(1, 1);
  lcd.print("GO GO GO !");
  tm.setLED(2, false);
  tm.setLEDs(0b00000000);
  digitalWrite(GREEN, HIGH);
  delay(500);
  lcd.clear();

  lcd.createChar(1, spaceShuttle);
  lcd.createChar(2, bomb);
  startTime2 = millis();
  gameRunning = true;
}

void moveSpaceShuttle() {
  int value = analogRead(JOYSTICK_Y_PIN);
  float normalizedValue = map(value, 0, 255, 0, 1); // normalize joystick input, anders detecteert hij sommige moves niet
  if (normalizedValue < 0.25 && spaceShuttlePosition > 0) {
    lcd.setCursor(0, spaceShuttlePosition);
    lcd.print(" ");
    spaceShuttlePosition--;
  } else if (normalizedValue >= 0.25 && normalizedValue < 0.75) {
    // niks, niemandsland
  } else if (normalizedValue >= 0.75 && spaceShuttlePosition < 1) {
    lcd.setCursor(0, spaceShuttlePosition);
    lcd.print(" ");
    spaceShuttlePosition++;
  }
  lcd.setCursor(0, spaceShuttlePosition);
  lcd.write(byte(1));
  delay(50);
}

// generate bombs with random rows
void generateBombs() {
  if (random(0, 30) == 0) {
    int index = random(0, 3);
    if (bombPosition[index] == -1) {
      int row = random(0, 2); // boven of onder
      int col = random(7, 16); // begin na de 7e plek anders instant crash
      bombPosition[index] = row == 0 ? col : col + 16;
    }
  }
}

void moveBombs() {
  for (int i = 0; i < 3; i++) {
    if (bombPosition[i] != -1) {
      int row = bombPosition[i] >= 16 ? 1 : 0;
      lcd.setCursor(bombPosition[i] - (row * 16) + 1, row);
      lcd.write(32);
      bombPosition[i]--;
      if (bombPosition[i] < 0) {
        bombPosition[i] = -1;
      } else {
        row = bombPosition[i] >= 16 ? 1 : 0;
        lcd.setCursor(bombPosition[i] - (row * 16) + 1, row);
        lcd.write(byte(2));
      }
      delay(50);
    }
  }
}
void checkCollision() {
  // check for collision
  for (int i = 0; i < 3; i++) {
    if (bombPosition[i] != -1 && gameRunning) {
      int bombRow = (bombPosition[i] >= 16) ? 1 : 0;
      int bombCol = bombPosition[i] - (bombRow * 16) + 1;

      // Check if the bomb is in the same row and column as the shuttle
      if (bombRow == spaceShuttlePosition && bombCol == 1) {
        gameRunning = false;
        lcd.setCursor(0, 0);
        lcd.print("Game over! ");
        lcd.setCursor(0, 1);
        lcd.print("Score: ");
        lcd.print(score2);
        delay(1000);
        game2();
      }
    }
  }
}

void updateScore() {
  if (gameRunning) {
    unsigned long currentTime = millis();
    score2 = (currentTime - startTime2) / 1000;
    if (score2 >= targetScore) {
      win();
    }
    lcd.setCursor(13, 0);
    lcd.print(score2);
  }
}

void win() {
  gameRunning = false;
  game2Done = true;
  displayWinningMessage();
}

// --------------------------------- GAME3 ---------------------------------------------------
// -------------------------------------------------------------------------------------------

const int REQUIRED_SCORE = 10;
const unsigned long TIME_LIMIT = 30000;  // 30 seconds

uint8_t currentLEDs[8] = { 0 };
int score = 0;
unsigned long lastLEDChange = 0;
unsigned long ledInterval = 2000;
uint8_t numActiveLEDs = 2;
unsigned long startTime3 = 0;
unsigned long lastTimeUpdate = 0;

void game3() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Welcome to:");
  lcd.setCursor(0, 1);
  lcd.print("Kill the ants!");
  delay(1000);
  lcd.clear();
  for (int i = 3; i > 0; i--) {
    lcd.setCursor(0, 1);
    lcd.print(i);
    delay(1000);
  }
  lcd.clear();

  startGame3();
}

void startGame3() {
  randomSeed(analogRead(0));
  activateRandomLEDs();
  lastLEDChange = millis();
  startTime3 = millis();
  lastTimeUpdate = millis();

  while (1) {
    uint8_t buttons = tm.readButtons();

    if (buttons != 0) {
      checkButtonPress(buttons);
    }

    if (millis() - lastLEDChange >= ledInterval) {
      deactivateCurrentLEDs();
      activateRandomLEDs();
      lastLEDChange = millis();
      if (ledInterval > 500) {
        ledInterval -= 50; // change interval
      }
      if (score % 10 == 0 && numActiveLEDs < 8) {
        numActiveLEDs++;
      }
    }

    if (millis() - lastTimeUpdate >= 1000) {
      updateRemainingTime();
      lastTimeUpdate = millis();
    }

    if (millis() - startTime3 >= TIME_LIMIT) {
      if (score >= REQUIRED_SCORE) {  //win win win
        displayWinningMessage();
        game3Done = true;
        break;
      } else {
        gameOver();
      }
    }

    delay(100);
  }
}

void activateRandomLEDs() {
  for (uint8_t i = 0; i < numActiveLEDs; i++) {
    uint8_t randomLED = random(0, 8);
    currentLEDs[i] = randomLED;
    tm.setLED(randomLED, 1);
  }
}

void updateRemainingTime() {
  int timeLeft = (TIME_LIMIT - (millis() - startTime3)) / 1000;
  tm.DisplayDecNumNibble(timeLeft, 0, true, TMAlignTextLeft);
}

void deactivateCurrentLEDs() {
  for (uint8_t i = 0; i < numActiveLEDs; i++) {
    tm.setLED(currentLEDs[i], 0);
  }
}

void checkButtonPress(uint8_t buttons) {
  static uint8_t lastButtons = 0;

  if (buttons != lastButtons) {
    lastButtons = buttons;
    uint8_t buttonMask = 1;
    for (uint8_t buttonIndex = 0; buttonIndex < 8; buttonIndex++) {
      if ((buttons & buttonMask) != 0) {
        for (uint8_t i = 0; i < numActiveLEDs; i++) {
          if (buttonIndex == currentLEDs[i]) {
            tm.setLED(currentLEDs[i], 0);
            currentLEDs[i] = 255;
            score++;
            updateScoreLCD();
          }
        }
        break;
      }
      buttonMask = buttonMask << 1;
    }
  }
}

void updateScoreLCD() {
  lcd.setCursor(0, 0);
  lcd.print("Score: ");
  lcd.print(score);
}

void gameOver() {
  deactivateCurrentLEDs();
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Game Over!");
  lcd.setCursor(0, 1);
  lcd.print("Score: ");
  lcd.print(score);
  delay(4000);
  game3();
}

// --------------------------------- GAME4 ---------------------------------------------------
// -------------------------------------------------------------------------------------------

int noteSequence[8] = { 262, 294, 330, 349, 392, 440, 494, 523 };
int currentNoteIndex = 0;
int score4 = 0;
const int target_score = 11;
unsigned long startTime4;
unsigned long timeLimit = 30000;  // 30 seconds
unsigned long currentFrequency;
int noteThreshold = 10;
bool game4Running = false;

void game4() {
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Welcome to:");
  lcd.setCursor(0, 1);
  lcd.print("Buzzer Matcher!");

  delay(2000);
  lcd.clear();

  delay(2000);
  lcd.clear();

  for (int i = 3; i > 0; i--) {
    lcd.setCursor(0, 1);
    lcd.print(i);
    delay(1000);
  }
  lcd.clear();

  while (!game4Done) {
    if (!game4Running) {
      startGame4();
    }
    if (game4Running) {
      checkPotentiometer();
    }
  }
}

void startGame4() {
  lcd.clear();
  game4Running = true;
  score4 = 0;
  updateScoreLCD4();
  startTime4 = millis();
  changeNotes();

  while (game4Running) {
    playNote(currentNoteIndex);
    unsigned long noteStart = millis();
    while (millis() - noteStart < 500) {
      checkPotentiometer();
      updateTimerLCD();
      delay(10);
    }

    if (score4 >= target_score) {
      winGame();
      break;
    }

    if (millis() - startTime4 >= timeLimit) {
      gameOver4();
    }
  }
}


void changeNotes() {
  for (int i = 0; i < 8; i++) {
    int randomIndex = random(i, 8);
    int temp = noteSequence[i];
    noteSequence[i] = noteSequence[randomIndex];
    noteSequence[randomIndex] = temp;
  }
}

void playNote(int noteIndex) {
  tone(BUZZER_PIN, noteSequence[noteIndex], 500);
}

void checkPotentiometer() {
  int potValue = analogRead(POTENTIOMETER_PIN);
  int frequency = map(potValue, 0, 1023, 250, 550);
  currentFrequency = frequency;
  Serial.println("frequency");
  Serial.println(potValue);
  updateFrequencyLCD();

  if (abs(frequency - noteSequence[currentNoteIndex]) <= noteThreshold) {
    score4++;
    updateScoreLCD4();
    currentNoteIndex++;
    if (currentNoteIndex >= 8) {
      currentNoteIndex = 0;
      changeNotes();
    }
  }
}

void updateScoreLCD4() {
  lcd.setCursor(0, 0);
  lcd.print("Score: ");
  lcd.print(score4);
}

void updateFrequencyLCD() {
  lcd.setCursor(0, 1);
  lcd.print("Freq: ");
  lcd.print(currentFrequency);
}

void updateTimerLCD() {
  int timeLeft = (timeLimit - (millis() - startTime4)) / 1000;
  lcd.setCursor(10, 1);
  lcd.print("T:");
  lcd.print(timeLeft);
  lcd.print("s");
}

void gameOver4() {
  game4Running = false;
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("Game Over, time up!      ");
  lcd.setCursor(0, 1);
  lcd.print("Score: ");
  lcd.print(score4);
  delay(5000);
  startGame4();
}

void winGame() {
  game4Running = false;
  lcd.clear();
  lcd.setCursor(0, 0);
  lcd.print("You won!");
  lcd.setCursor(0, 1);
  lcd.print("Score: ");
  lcd.print(score4);
  noTone(BUZZER_PIN);
  game4Done = true;
  delay(2000);
}
